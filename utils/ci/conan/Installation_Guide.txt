################################################################################
# Copyright (c) 2021 ITK Engineering GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

1. Install MSYS2 64-bit to C:/msys64 (default)
    https://www.msys2.org/

2. Open MSYS2 Shell and execute the following commands. Reply with "yes" or "return" if necessary.
    - $ yes | pacman -Syu (repeat until no additional packages where installed)
    - navigate with "cd" to script folder
    - $ sh 10_prepare_msys2.sh

3. Open MSYS2 MINGW64 Shell and execute the following commands. Reply with "yes" or "return" if necessary.
    - $ sh 15_prepare_mingw.sh

4. Configure Conan and build openpass with the following command
    - $ sh 20_build.sh

5. Add to your PATH environment variables:
    - "c:\msys64\mingw64\bin"
    - "c:\msys64\mingw64\lib"
    - "c:\msys64\usr\bin"

6. Add new environment variables:
    - CC = "c:/msys64/mingw64/bin/gcc.exe"
    - CXX = "c:/msys64/mingw64/bin/g++.exe"
